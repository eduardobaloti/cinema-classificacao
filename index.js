const express = require("express");
const axios = require("axios")
const app = express();
app.use(express.json());
const palavraChave = "importante";

const funcoes = {
    IngressoCriado: (ingresso) => {
        ingresso.status =
            ingresso.texto.includes(palavraChave) ?
            "importante" :
            "comum";
        axios.post('http://192.168.16.1:10000/eventos', {
            tipo: "IngressoClassificado",
            dados: ingresso,
        }).catch((err) => {
            console.log("err", err);
        });
    },
};

app.post('/eventos', (req, res) => {
    try {
        funcoes[req.body.tipo](req.body.dados);
    } catch (err) {}
    res.status(200).send({
        msg: "ok"
    });
});

app.listen(8000, () => console.log("Classificação. Porta 8000"));